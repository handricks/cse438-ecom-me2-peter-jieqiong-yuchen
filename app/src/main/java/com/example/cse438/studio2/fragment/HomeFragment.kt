package com.example.cse438.studio2.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cse438.studio2.R
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.whats_new_list_item.view.*

@SuppressLint("ValidFragment")
class HomeFragment(context: Context): Fragment() {
    private var adapter = NewProductAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: ProductViewModel

    private var ProductList: ArrayList<Product> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onStart() {
        super.onStart()
        new_items_list.layoutManager = LinearLayoutManager(parentContext)
        new_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        val observer = Observer<ArrayList<Product>> {
            new_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return ProductList[p0].getId() == ProductList[p1].getId()
                }

                override fun getOldListSize(): Int {
                    return ProductList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return ProductList[p0] == ProductList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            ProductList = it ?: ArrayList()
        }

        viewModel.getNewProducts().observe(this, observer)
    }

    inner class NewProductAdapter: RecyclerView.Adapter<NewProductAdapter.NewProductViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewProductViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.whats_new_list_item, p0, false)
            return NewProductViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: NewProductViewHolder, p1: Int) {
            val product = ProductList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@HomeFragment.context).load(productImages[0]).into(p0.productImg)
            }
            p0.productTitle.text = product.getProductName()
        }

        override fun getItemCount(): Int {
            return ProductList.size
        }

        inner class NewProductViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
        }
    }
}